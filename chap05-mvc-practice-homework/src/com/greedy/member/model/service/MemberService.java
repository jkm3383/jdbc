package com.greedy.member.model.service;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.List;

import com.greedy.member.model.dao.MemberDAO;
import com.greedy.member.model.dto.MemberDTO;

public class MemberService {
	private MemberDAO memberDAO = new MemberDAO();
	
	public List<MemberDTO> selectAllmember() {
		Connection con = getConnection();
		
		List<MemberDTO> memberList = memberDAO.selectAllmembers(con);
		
		close(con);
		
		return memberList;
	}

	public int registNewMember(MemberDTO memberDTO) {
		Connection con = getConnection();
		
		int result = memberDAO.registNewMember(memberDTO, con);
		
		close(con);
		
		return result;
	}

	public MemberDTO searchMemberById(String id) {
		Connection con = getConnection();
		
		MemberDTO memberDTO = memberDAO.searchMemberById(id, con);
		
		close(con);
		
		return memberDTO;
	}

	public List<MemberDTO> searchMemberByGender(String gender) {
		Connection con = getConnection();
		
		List<MemberDTO> memberList = memberDAO.searchMemberByGender(gender, con);
		
		close(con);
		
		return memberList;
	}

	public int modifyPassword(String memberId, String password) {
		Connection con = getConnection();
		
		int result = memberDAO.modifyPassword(memberId, password, con);
		
		close(con);
		
		return result;
	}

	public int modifyEmail(String memberId, String email) {
		Connection con = getConnection();
		
		int result = memberDAO.modifyEmail(memberId, email, con);
		
		close(con);
		
		return result;
	}

	public int modifyPhone(String memberId, String phone) {
		Connection con = getConnection();
		
		int result = memberDAO.modifyPhone(memberId, phone, con);
		
		close(con);
		
		return result;
	}

	public int modifyAddress(String memberId, String address) {
		Connection con = getConnection();
		
		int result = memberDAO.modifyAddress(memberId, address, con);
		
		close(con);
		
		return result;
	}

	public int deleteMember(String memberId) {
		
		Connection con = getConnection();
		
		int result = memberDAO.deleteMember(memberId, con);
		
		close(con);
		
		return result;
	}

}
