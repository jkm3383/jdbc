package com.greedy.section01.run;

import com.greedy.section01.view.OrderMenu;

public class Application {

	public static void main(String[] args) {
		
		// 1단계 : 오더메뉴 객체 생성
		OrderMenu orderMenu = new OrderMenu();
		// 2단계 : 디스플레이 메인 메뉴 메소드 호출
		orderMenu.displayMainMenu();
	}
}