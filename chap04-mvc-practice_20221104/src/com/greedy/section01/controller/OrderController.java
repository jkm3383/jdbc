package com.greedy.section01.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;
import com.greedy.section01.model.service.OrderService;
import com.greedy.section01.view.ResultView;

public class OrderController {

	// 8단계 : 필드부 작성
	private OrderService orderService = new OrderService();
	
	// 7단계 : 퍼블릭 작성
	public List<CategoryDTO> selectAllCategory() {
		
		List<CategoryDTO> categoryList = orderService.selectAllCategory();
		
		// 15단계 : 리턴 값 작성
		return categoryList;
	}

	// 18단계 : 메소드 생성
	public List<MenuDTO> seletMenuBy(String inputCategory, List<CategoryDTO> categoryList) {
		
		// 19단계 : 사용자가 입력한 이름에 맞는 카테고리 코드가 담길 변수 생성
		int categoryCode = 0;
		// 19단계 : 사용자가 입력한 카테고리 이름과 일치하면 해당 카테고리의 코드 번호를 변수(categoryCode)에 대입 
		for(CategoryDTO category : categoryList) {
			if(category.getCategoryName().equals(inputCategory)) {
				categoryCode = category.getCategoryCode();
			}
		}
		// 20단계 : 서비스에 카테고리 코드 넘기면서 메소드 생성
		return orderService.seletMenuBy(categoryCode);
	}

	// 24단계 : 메소드 생성
	public void registOrder(Map<String, Object> requestMap) {
		// 24단계 : 뷰에서 전달받은 파라미터를 꺼내서 각각의 변수에 담기
		// ↓ Object 다운 캐스팅 수행
		int totalOrderPrice = (Integer)requestMap.get("totalOrderPrice");
		List<OrderMenuDTO> orderMenuList = (List<OrderMenuDTO>)requestMap.get("orderMenuList");
		
		// 24단계 : 화면단에서 미처 제공하지 못했거나 가공 처리해야 할 것들을 가공 처리함
		// 		   주문 날짜와 시간을 Controller(서버 시간)에서 구하기
		java.util.Date orderTime = new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		String date = dateFormat.format(orderTime);
		String time = timeFormat.format(orderTime);
		
		// 24단계 : OrderDTO 내용 작성
		// 		   서비스 쪽으로 전달하기 위해 DTO 인스턴스 생성 (묶기)
		OrderDTO order = new OrderDTO();
		order.setOrderDate(date);
		order.setOrderTime(time);
		order.setTotalOrderPrice(totalOrderPrice);
		
		// 24단계 : Service 메소드를 호출하고 결과를 리턴 받음 => 메소드 생성
		int result = orderService.registOrder(order, orderMenuList);
		
		// 32단계 : 반환받은 실행 결과를 토대로 사용자에게 보여줄 결과 세팅
		// 		   서비스에서 제대로 수행된 경우 => result = 1
		//		   서비스에서 제대로 수행되지 않은 경우 => result = 0
		//		   사용자 환경의 개선을 위해 resultView 클래스를 새로이 만들어줌
		if(result > 0) {
			ResultView.success(orderMenuList.size());
		} else {
			ResultView.failed();
		}
	}
}