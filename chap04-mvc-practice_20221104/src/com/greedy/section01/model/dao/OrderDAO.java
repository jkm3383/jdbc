package com.greedy.section01.model.dao;

import static com.greedy.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderDAO {

	public List<CategoryDTO> selectAllCategory(Connection con) {
		
		// 11단계 : 리스트 생성 -> DTO 작성
		CategoryDTO category = null;
		List<CategoryDTO> categoryList = new ArrayList<>();
		
		// 12단계 : 쿼리 작성
		String query = "SELECT\r\n"
				+ "       A.CATEGORY_CODE\r\n"
				+ "     , A.CATEGORY_NAME\r\n"
				+ "     , A.REF_CATEGORY_CODE\r\n"
				+ "  FROM TBL_CATEGORY A";
		
		// 13단계 : 트럭 및 결과값 세팅
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			// 13단계 : 결과값 세팅
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				category = new CategoryDTO();
				category.setCategoryCode(rset.getInt("CATEGORY_CODE"));
				category.setCategoryName(rset.getString("CATEGORY_NAME"));
				category.setRefCategoryCode(rset.getInt("REF_CATEGORY_CODE"));
				
				// 13단계 : 결과값 담기
				categoryList.add(category);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 14단계 : 리소스 반납
			close(rset);
			close(pstmt);
		}
		// 15단계 : 리턴 값 작성
		return categoryList;
	}

	// 20단계 : 메소드 생성
	public List<MenuDTO> selectMenuBy(int categoryCode, Connection con) {
		// 20단계 : 트럭 및 결과 리스트 받을 리줄트 생성
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		// 20단계 : 와일문에서 사용하기 위해 MenuDTO를 담기위한 menu 인스턴스 생성
		MenuDTO menu = null;
		// 20단계 : 리스트 선언 및 초기화
		List<MenuDTO> menuList = new ArrayList<>();
		
		// 20단계 : 프로퍼티 생성
		Properties prop = new Properties();
		
		try {
			// 20단계 : XML 파일 읽기
			prop.loadFromXML(new FileInputStream("mapper/order-query.xml"));
			// 20단계 : 쿼리 뽑기 
			String query = prop.getProperty("selectMenuByCategory");
			// 20단계 : 트럭 보수공사 (with 쿼리)
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, categoryCode);
			
			// 20단계 : 트럭에 실린 값 리줄트에 넣기
			rset = pstmt.executeQuery();
			
			// 20단계 : 뽑을 것이 여러개라서 while 문 돌림
			while(rset.next()) {
				// 20단계 : 메뉴객체에 DTO 담기
				menu = new MenuDTO();
				
				// 20단계 : DTO의 세터와 게터를 이용하여 매핑
				menu.setMenuCode(rset.getInt("MENU_CODE"));
				menu.setMenuName(rset.getString("MENU_NAME"));
				menu.setMenuPrice(rset.getInt("MENU_PRICE"));
				menu.setCategoryCode(rset.getInt("CATEGORY_CODE"));
				menu.setOrderableStatus(rset.getString("ORDERABLE_STATUS"));
				
				// 20단계 : 리스트에 매핑된 객체들 담기
				menuList.add(menu);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 20단계 : 자원 반납
			close(rset);
			close(pstmt);
		}
		// 20단계 : 리스트 반환
		return menuList;
	}

	// 26단계 : 메소드 생성
	public int registOrder(OrderDTO order, Connection con) {
		// 26단계 : 트럭 생성 및 DML 작업이므로 int 값으로 리줄트 담기
		PreparedStatement pstmt = null;
		int result = 0;
		
		// 26단계 : 프로퍼티 인스턴스 생성
		Properties prop = new Properties();
		
		try {
			// 26단계 : XML 불러오기
			prop.loadFromXML(new FileInputStream("mapper/order-query.xml"));
			// 26단계 : 쿼리 담기
			String query = prop.getProperty("insertOrder");
			// 26단계 : 쿼리에 매핑
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, order.getOrderDate());
			pstmt.setString(2, order.getOrderTime());
			pstmt.setInt(3, order.getTotalOrderPrice());
			
			// 26단계 : 매핑된 값 리줄트에 담기 [DML 수행 => executeUpdate -> int 반환]
			result = pstmt.executeUpdate();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 26단계 : 자원 반납
			close(pstmt);
		}
		// 26단계 : 리줄트 반환 => 호출한 곳으로 돌아감(서비스)
		return result;
	}

	// 27단계 : 메소드 생성
	public int lastOrderCode(Connection con) {
		// 27단계 : 트럭 생성 및 리줄트 값 선언 및 초기화
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		// 27단계 : 시퀀스 값 담을 변수 선언
		int lastOrderCode = 0;
		
		// 27단계 : 프로퍼티 인스턴스 생성
		Properties prop = new Properties();
		
		try {
			// 27단계 : XML 불러오기 => 쿼리 담기 => 쿼리 매핑 (최근 시퀀스 값)
			prop.loadFromXML(new FileInputStream("mapper/order-query.xml"));
			String query = prop.getProperty("selectLastOrderCode");
			
			pstmt = con.prepareStatement(query);
			
			// 27단계 : 리줄트 담기 [조회 => executeQuery -> ResultSet 반환]
			rset = pstmt.executeQuery();
			
			// 27단계 : DB에서 실행된 쿼리의 단일 행 결과 값을 담기 위해 if문 작성
			if(rset.next()) {
				// 참고 : 첫 번째 컬럼값을 담기 위해 getInt(1)작성
				lastOrderCode = rset.getInt(1);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 27단계 : 자원 반납
			close(rset);
			close(pstmt);
		}
		// 27단계 : 서비스 계층에 리줄트(lastOrderCode) 반환 => 서비스로 복귀
		return lastOrderCode;
	}

	// 28단계 : 메소드 생성
	public int registOrderMenu(OrderMenuDTO orderMenuDTO, Connection con) {
		// 28단계 : 트럭생성 및 DML수행 작업을 담을 리즐트 선언 및 초기화 
		PreparedStatement pstmt = null;
		int result = 0;
		
		// 28단계 : 프로퍼티 인스턴스 생성
		Properties prop = new Properties();
		try {
			// 28단계 : XML 불러오기 => 쿼리 담기 => 쿼리 매핑
			prop.loadFromXML(new FileInputStream("mapper/order-query.xml"));
			String query = prop.getProperty("insertOrderMenu");
			
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, orderMenuDTO.getOrderCode());
			pstmt.setInt(2, orderMenuDTO.getMenuCode());
			pstmt.setInt(3, orderMenuDTO.getOrderAmount());
			
			// 28단계 : 리줄트에 쿼리 실행 값 담기
			result = pstmt.executeUpdate();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 28단계 : 자원 반납			
			close(pstmt);
		}
		// 28단계 : 리줄트 값(DML 실행된 행의 개수) 반환 => 서비스 탭에서 누적될 값 => 서비스로 복귀
		return result;
	}
}