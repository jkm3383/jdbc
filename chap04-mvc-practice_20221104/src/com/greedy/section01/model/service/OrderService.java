package com.greedy.section01.model.service;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.commit;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.List;

import com.greedy.section01.model.dao.OrderDAO;
import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

/*
 * ▼ Service Layer(계층)의 역할
 * 1. Connection 생성
 * 2. DAO 계층의 메소드 호출
 * 3. 트랜잭션 제어 (DML일 시, COMMIT or ROLLBACK)
 * 4. Connection 닫기 => 리소스 반납 
 */
public class OrderService {

	// 10단계 : 필드부 작성 
	private OrderDAO orderDAO = new OrderDAO();
	
	public List<CategoryDTO> selectAllCategory() {
		
		// 9단계 : 터널 생성
		Connection con = getConnection();
		
		// 9단계 : 리스트 생성 
		List<CategoryDTO> categoryList = orderDAO.selectAllCategory(con);
		
		// 9단계 : 터널 닫기
		close(con);
		
		// 15단계 : 리턴 값 작성
		return categoryList;
	}

	// 20단계 : 메소드 생성
	public List<MenuDTO> seletMenuBy(int categoryCode) {
		Connection con = getConnection();
		
		// 20단계 : 메뉴 DTO를 가지는 리스트를 생성
		List<MenuDTO> menuList = orderDAO.selectMenuBy(categoryCode, con);
		
		close(con);
		
		// 20단계 : 메뉴리스트 반환
		return menuList;
	}

	// 25단계 : 메소드 생성
	public int registOrder(OrderDTO order, List<OrderMenuDTO> orderMenuList) {
		// 25단계 : Controller에게 트랜잭션 성공 여부를 알려줄 변수
		// 		   주문 트랜잭션이 모두 성공적으로 마무리되면 1을 대입 받을 변수 (flag 개념)
		int result = 0;
		
		// 25단계 : Connection 생성
		Connection con = getConnection();
		
		// 26단계 : TBL_ORDER 테이블(부모)에 INSERT 수행 => OrderDAO에 메소드 생성
		int result1 = orderDAO.registOrder(order, con);
		System.out.println("order 테이블 insert 성공 여부 : " + result1);
		
		// 27단계 : 마지막 주문 시퀀스 조회(셀렉트 구문) => 메소드 생성
		// 참고 => 서비스의 쿼리 1줄 = DAO의 메소드 1개
		int orderCode = orderDAO.lastOrderCode(con);
		System.out.println("방금 추가한 주문 코드 : " + orderCode);
		
		// 28단계 : TBL_ORDER_MENU 테이블(자식)에 INSERT 수행
		int result2 = 0;
		
		// 28단계 : 사용자가 고른(orderMenuList에 담긴) 메뉴 개수만큼 메뉴별로 insert를 하고 결과를 누적해야 함
		for(int i = 0; i < orderMenuList.size(); i++) {
			// 28단계 : orderCode 세팅
			//	       각 메뉴 정보를 가진 orderMenuList.get(i)(=> OrderMenuDTO)에 주문 코드번호를 넣어주어야 함
			orderMenuList.get(i).setOrderCode(orderCode);
			// 28단계 : orderMenuList.get(i)와 커넥션을 받을 메소드 생성
			result2 += orderDAO.registOrderMenu(orderMenuList.get(i), con);
		}
		System.out.println("사용자가 고른 메뉴 개수만큼 성공한 결과 : " + result2);
		
		// 29단계 : 트랜잭션 처리
		// 		   SELECT는 COMMIT, ROLLBACK과 무관
		// 		   ∴ SELECT를 제외한 모든 DML 작업 성공 시의 조건 => if 문의 조건식에 작성
		if(result1 == 1 && result2 == orderMenuList.size()) {
			// ↑ 인서트 성공(1) && 사용자고른 메뉴개수만큼 성공햇는지여부
			// 29단계 : 둘다 만족하면 커밋
			commit(con);
			System.out.println("모든 DML 작업 성공!");
			
			// 29단계 : 25단계에서 선언한 트랜잭션 성공 여부를 반환하기 위해 1 대입해줌 
			result = 1;
		} else {
			// 29단계 : 둘 중 하나라도 만족 못하면 롤백
			rollback(con);
			System.out.println("DML 작업 중 하나라도 실패!");
		}
		// 30단계 : 자원 반납
		close(con);
		
		// 31단계 : 컨트롤러에게 실행 결과 반환 => 컨트롤러로 복귀
		return result;
	}
}