package com.greedy.section01.model.dto;

import java.io.Serializable;

public class OrderDTO implements Serializable{

	private static final long serialVersionUID = 605576496189669759L;
//	ORDER_CODE	NUMBER
//	ORDER_DATE	VARCHAR2(8 BYTE)
//	ORDER_TIME	VARCHAR2(8 BYTE)
//	TOTAL_ORDER_PRICE	NUMBER
	private int orderCode;
	private String orderDate;
	private String orderTime;
	private int totalOrderPrice;
	
	public OrderDTO() {
		super();
	}
	
	public OrderDTO(int orderCode, String orderDate, String orderTime, int totalOrderPrice) {
		super();
		this.orderCode = orderCode;
		this.orderDate = orderDate;
		this.orderTime = orderTime;
		this.totalOrderPrice = totalOrderPrice;
	}
	
	public int getOrderCode() {
		return orderCode;
	}
	
	public void setOrderCode(int orderCode) {
		this.orderCode = orderCode;
	}
	
	public String getOrderDate() {
		return orderDate;
	}
	
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	
	public String getOrderTime() {
		return orderTime;
	}
	
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	
	public int getTotalOrderPrice() {
		return totalOrderPrice;
	}
	
	public void setTotalOrderPrice(int totalOrderPrice) {
		this.totalOrderPrice = totalOrderPrice;
	}
	
	@Override
	public String toString() {
		return "OrderDTO [orderCode=" + orderCode + ", orderDate=" + orderDate + ", orderTime=" + orderTime
				+ ", totalOrderPrice=" + totalOrderPrice + "]";
	}
}