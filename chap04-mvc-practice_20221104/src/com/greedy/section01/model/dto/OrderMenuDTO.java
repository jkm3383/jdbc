package com.greedy.section01.model.dto;

import java.io.Serializable;

public class OrderMenuDTO implements Serializable{

	private static final long serialVersionUID = 2996900060514402566L;
//	ORDER_CODE	NUMBER
//	MENU_CODE	NUMBER
//	ORDER_AMOUNT	NUMBER
	private int orderCode;
	private int menuCode;
	private int orderAmount;
	
	public OrderMenuDTO() {
		super();
	}

	public OrderMenuDTO(int orderCode, int menuCode, int orderAmount) {
		super();
		this.orderCode = orderCode;
		this.menuCode = menuCode;
		this.orderAmount = orderAmount;
	}

	public int getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(int orderCode) {
		this.orderCode = orderCode;
	}

	public int getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(int menuCode) {
		this.menuCode = menuCode;
	}

	public int getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(int orderAmount) {
		this.orderAmount = orderAmount;
	}

	@Override
	public String toString() {
		return "OrderMenuDTO [orderCode=" + orderCode + ", menuCode=" + menuCode + ", orderAmount=" + orderAmount + "]";
	}
}