package com.greedy.section01.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.greedy.section01.controller.OrderController;
import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderMenu {
	
	// 5단계 : private OrderController orderController = new OrderController(); 타이핑 후 임포트
	private OrderController orderController = new OrderController();

	public void displayMainMenu() {
		
		// 17단계
		Scanner sc = new Scanner(System.in);
		
		// 22단계
		// [C] 주문 수량은 누적시킬 것 => do while 밖에 선언 (totalOrderPrice)
		int totalOrderPrice = 0;
		
		// 23단계 : (아직) 주문번호가 나오지 않았지만 해당 주문 메뉴에 대한 메뉴 코드 및 수량은 알 수 있음 => 매핑
		//		   => OrderMenuDTO 생성
		List<OrderMenuDTO> orderMenuList = new ArrayList<>();
		
		// 23단계
		// [D] 와일문 벗어날 플래그 작성
		boolean flag = true;
		
		// 3단계 : 두 와일 작성 (한번만 돌게)
		do {
			System.out.println("===== 음식 주문 프로그램 =====");
			
			// 4단계 : 리스트 생성 -> CategoryDTO 클래스 직접 생성
			// 6단계 : 오더컨트롤러 생성
			List<CategoryDTO> categoryList = orderController.selectAllCategory();
			
			// 15단계 : 리턴 값 작성
			for(CategoryDTO category : categoryList) {
				System.out.println(category);
			}
			
			// 17단계 : 주문할 카테고리 종류 입력
			System.out.println("주문할 카테고리 종류 이름 입력 : ");
			String inputCategory = sc.nextLine();
			
			// 18단계 : 카테고리 메뉴 출력 => DTO 생성 => 메소드 생성
			System.out.println("===== 주문 가능 해당 카테고리 메뉴 =====");
			List<MenuDTO> menuList = orderController.seletMenuBy(inputCategory, categoryList);
			
			// 18단계 : for each문으로 주문가능한 메뉴리스트 확인 
			/* for each 문 활용 */
			for(MenuDTO menu : menuList) {
				System.out.println(menu);
			}
			
/* [화면단에서 진행하는 과정] 메뉴이름 받아서 코드랑 가격매핑하고, 수량도 매핑하고 가격도 총계내서 전달 */
			// 21단계 : 주문할 메뉴(이름) 입력
			System.out.println("주문할 메뉴 입력 : ");
			String inputMenu = sc.nextLine();
			
			// 21단계 : 메뉴 이름이 정해지면 주문 관련에 쓰일 값들을 화면단에서는 이미 알고 있으므로 미리 추출해 둠
			// 화면단에서 처리해야 왔다갔다를 반복하지 않고 깔끔하게 작성할 수 있음
			/* for 문 활용 */
			int menuCode = 0;
			int menuPrice = 0;
			
			// 21단계 : 사용자가 입력한 카테고리안의 메뉴 사이즈로 반복문을 돌림
			//		   => 내가 입력한 메뉴와 같다면 => 코드와 가격을 매핑
			for(int i = 0; i < menuList.size(); i++) {
				MenuDTO menu = menuList.get(i);
				if(menu.getMenuName().equals(inputMenu)) {
					menuCode = menu.getMenuCode();
					menuPrice = menu.getMenuPrice();
				}
			}
			
			// 21단계 : [부가적 코드] 메뉴 이름을 잘못 입력할 경우 과정이 진행되지 않고 다시 메뉴를 입력하게 함
			if(menuPrice == 0) {
				System.out.println("해당 메뉴가 없습니다.");
				continue;
			}
			
			// 22단계 : 주문할 수량 입력
			System.out.println("주문할 수량 입력 : ");
			int orderAmount = sc.nextInt();
			sc.nextLine();
			
			// 22단계 : 메뉴 한건 주문에 대한 합계를 전체 합계에 누적시킴
			// [C] 주문 수량은 누적시킬 것 => do while 밖에 선언 (totalOrderPrice)
			totalOrderPrice += menuPrice * orderAmount;
			
			// 23단계 : (아직) 주문번호가 나오지 않았지만 해당 주문 메뉴에 대한 메뉴 코드 및 수량은 알 수 있음 => 매핑
			//		   => OrderMenuDTO 생성
			OrderMenuDTO orderMenu = new OrderMenuDTO();
			orderMenu.setMenuCode(menuCode);
			orderMenu.setOrderAmount(orderAmount);
			
			// 23단계 : 주문 메뉴(메뉴코드, 메뉴주문수량)를 List에 누적시킴
			orderMenuList.add(orderMenu);
			
			// 23단계 : 메뉴 추가 선택 여부를 결정하기 위한 코드 (예/아니오 입력받음)
			String isContinue = "";
			while(true) {
				System.out.println("추가 주문 여부 (예/아니오) : ");
				isContinue = sc.nextLine();
				if("예".equals(isContinue)) {
					break;
				} else if("아니오".equals(isContinue)) {
					// 23단계
					// [D] 와일문 벗어날 플래그 작성
					flag = false;
					break;
				}
			}
//		} while(false);
		// 23단계
		// [D] 와일문 벗어날 플래그 작성
		} while(flag);
		
		// 24단계 : 컨트롤러에 전달할 Map 작성 => 가격과 주문한 메뉴 리스트 매핑 후 전달
		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("totalOrderPrice", totalOrderPrice);
		requestMap.put("orderMenuList", orderMenuList);
		
		// 24단계 : 메소드 생성
		orderController.registOrder(requestMap);
	}
	
}